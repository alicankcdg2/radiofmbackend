package com.first.radio.RadioFM;

import com.first.radio.RadioFM.base.GlobalResponseCodes;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication()
public class RadioFmApplication {

	public static void main(String[] args) {
		SpringApplication.run(RadioFmApplication.class, args);
		GlobalResponseCodes.putResponseMessages();
	}

}

package com.first.radio.RadioFM.base;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.first.radio.RadioFM.model.data.AppLog;
import com.first.radio.RadioFM.service.applog.AppLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ApiInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (request.getServletPath().contains("static/img") ||
                request.getMethod().equals(HttpMethod.GET.name())) {
            return true;
        }
        String token = request.getHeader("App_Token");
        String countryCode = request.getHeader("Country_Code");
        if ((token == null || token.isEmpty()) || (countryCode == null || countryCode.isEmpty())) {
            throw new TokenExpiredException("Invalid Request");
        }
        String subject = JWT.decode(request.getHeader("App_Token")).getSubject();
        if (!subject.equals("test"))
        throw new TokenExpiredException("Invalid Request");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }


}

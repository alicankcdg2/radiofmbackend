package com.first.radio.RadioFM.base;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import java.util.Date;

public class BaseController {

    public BaseController() {

        System.out.println("TEST");
    }

    private String createAccessToken(String deviceId,String issuer) {
        Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());

        String accessToken = JWT.create()
                .withSubject(deviceId)
                .withExpiresAt(new Date(System.currentTimeMillis() + 10*20*1000))
                .withIssuer(issuer)
                .sign(algorithm);

        return accessToken;

    }
}

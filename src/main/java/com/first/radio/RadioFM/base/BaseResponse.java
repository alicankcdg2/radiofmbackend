package com.first.radio.RadioFM.base;

public class BaseResponse {
    int status;
    String message;
    Object data;

    public BaseResponse (Integer status) {
        setStatus(status);
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public BaseResponse() {}

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        setMessage(GlobalResponseCodes.getResponseMessage(status));
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

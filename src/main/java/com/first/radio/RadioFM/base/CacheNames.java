package com.first.radio.RadioFM.base;

public enum CacheNames {
    HOME_RADIO_LIST {
        @Override
        public String toString() {
            return "homeRadioList";
        }
    }
}

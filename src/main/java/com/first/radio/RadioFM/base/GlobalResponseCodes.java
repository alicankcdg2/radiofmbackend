package com.first.radio.RadioFM.base;

import java.util.HashMap;

public class GlobalResponseCodes {
    public static HashMap<Integer,String> responseMessages = new HashMap<>();

    static HashMap<Integer,String>getResponseMessages() {
        return responseMessages;
    }

    public static void putResponseMessages() {
        responseMessages.put(200,"Success");
        responseMessages.put(601,"Invalid App Key");
        responseMessages.put(602,"Data not Handled");
        responseMessages.put(603,"Check Required Fields");
        responseMessages.put(604,"Message Not Saved");
    }

    public static String getResponseMessage(Integer key) {
        return getResponseMessages().containsKey(key) ? getResponseMessages().get(key) : String.valueOf(key);
    }
}

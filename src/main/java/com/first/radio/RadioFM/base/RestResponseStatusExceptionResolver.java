package com.first.radio.RadioFM.base;

import com.auth0.jwt.exceptions.TokenExpiredException;
import com.fasterxml.jackson.databind.ser.Serializers;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class RestResponseStatusExceptionResolver extends ResponseEntityExceptionHandler {



    @ExceptionHandler(value
            = { TokenExpiredException.class, IllegalStateException.class })
    protected ResponseEntity<Object> handleConflict(
            RuntimeException ex, WebRequest request) {
        BaseResponse baseResponse = new BaseResponse();
        if (ex instanceof TokenExpiredException) {
            baseResponse.setStatus(601);
        }
        return handleExceptionInternal(ex, baseResponse,
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
}

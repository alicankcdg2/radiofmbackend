package com.first.radio.RadioFM.controller;

import jdk.jfr.ContentType;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;

@Controller
public class RadioWebController {
    ServletContext servletContext;

    @GetMapping("/radio")
    public String radio() {
        return "index";
    }

    @GetMapping("/radio1")
    @ResponseBody
    public StreamingResponseBody getVidoeStream1(HttpServletResponse response) throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        InputStream responseEntity = restTemplate.execute(URI.create("https://radyo.duhnet.tv/slowturk"), HttpMethod.GET,
                clientHttpRequest -> {}, clientHttpResponse -> {
                    return clientHttpResponse.getBody();
                });
        InputStream st = responseEntity;
        return (os) -> {
            readAndWrite(st, os);
        };

    }



    private void readAndWrite(final InputStream is, OutputStream os)
            throws IOException {
        byte[] data = new byte[2048];
        int read = 0;
        while ((read = is.read(data)) > 0) {
            os.write(data, 0, read);
        }
        os.flush();
    }
}

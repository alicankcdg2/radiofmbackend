package com.first.radio.RadioFM.controller.cache;

import com.first.radio.RadioFM.base.BaseResponse;
import com.first.radio.RadioFM.model.input.CacheControlInputModel;
import com.first.radio.RadioFM.model.output.CacheControlOutputModel;
import com.first.radio.RadioFM.service.cache.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/cache")
@RestController
public class CacheController implements ICacheController{

    @Autowired
    private CacheService cacheService;

    @Override
    @PostMapping("/control")
    public ResponseEntity<BaseResponse> cacheControl(@RequestHeader("Country_Code") String countryCode, @RequestBody CacheControlInputModel cacheControlInputModel) {
        BaseResponse baseResponse = new BaseResponse();
        boolean isCacheChange = cacheService.isCacheChange(cacheControlInputModel.getCacheCode(),countryCode, cacheControlInputModel.getName());
        CacheControlOutputModel cacheControlOutputModel = new CacheControlOutputModel();
        cacheControlOutputModel.cacheChanged = isCacheChange;
        baseResponse.setStatus(200);
        baseResponse.setData(cacheControlOutputModel);
        return ResponseEntity.ok(baseResponse);
    }
}

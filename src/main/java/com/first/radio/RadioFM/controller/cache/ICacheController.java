package com.first.radio.RadioFM.controller.cache;

import com.first.radio.RadioFM.base.BaseResponse;
import com.first.radio.RadioFM.model.input.CacheControlInputModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

public interface ICacheController {
    ResponseEntity<BaseResponse> cacheControl(@RequestHeader String countryCode, @RequestBody CacheControlInputModel cacheControlInputModel);
}

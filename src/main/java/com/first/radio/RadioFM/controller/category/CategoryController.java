package com.first.radio.RadioFM.controller.category;

import com.first.radio.RadioFM.base.BaseController;
import com.first.radio.RadioFM.model.data.Category;
import com.first.radio.RadioFM.model.output.CategoryOutputModel;
import com.first.radio.RadioFM.service.category.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/api/category")
@RestController
public class CategoryController extends BaseController implements ICategoryController{

    @Autowired
    ICategoryService categoryService;

    @GetMapping("/list")
    @Override
    public ResponseEntity<CategoryOutputModel> getCategoryList() {
        CategoryOutputModel categories = categoryService.getCategoryList();
        return ResponseEntity.ok(categories);
    }
}

package com.first.radio.RadioFM.controller.category;

import com.first.radio.RadioFM.model.data.Category;
import com.first.radio.RadioFM.model.output.CategoryOutputModel;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ICategoryController {
    public ResponseEntity<CategoryOutputModel> getCategoryList();
}

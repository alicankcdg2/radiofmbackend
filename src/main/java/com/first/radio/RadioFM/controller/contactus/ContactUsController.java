package com.first.radio.RadioFM.controller.contactus;

import com.first.radio.RadioFM.base.BaseResponse;
import com.first.radio.RadioFM.model.input.ContactUsInputModel;
import com.first.radio.RadioFM.model.output.ContactUsOutputModel;
import com.first.radio.RadioFM.service.contactus.ContactUsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/contact-us")

public class ContactUsController implements IContactUsController {


    @Autowired
    ContactUsService contactUsService;


    @Override
    @PostMapping(value = "/save",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> contactUs(@RequestBody ContactUsInputModel contactUsInputModel) {
        BaseResponse baseResponse = new BaseResponse();
        ContactUsOutputModel contactUsOutputModel = new ContactUsOutputModel();
        if (contactUsInputModel.getSubject().isBlank()
                || contactUsInputModel.getContent().isEmpty()) {
            baseResponse.setStatus(603);
        } else {
            contactUsOutputModel.setSaved(contactUsService.saveMessage(contactUsInputModel.getSubject(),contactUsInputModel.getContent()));
            baseResponse.setData(contactUsOutputModel);
            if (contactUsOutputModel.isSaved()) {
                baseResponse.setStatus(200);
            } else {
                baseResponse.setStatus(604);
            }
        }

        return ResponseEntity.ok(baseResponse);
    }
}

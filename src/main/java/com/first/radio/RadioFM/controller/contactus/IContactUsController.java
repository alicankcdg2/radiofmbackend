package com.first.radio.RadioFM.controller.contactus;

import com.first.radio.RadioFM.base.BaseResponse;
import com.first.radio.RadioFM.model.input.ContactUsInputModel;
import org.springframework.http.ResponseEntity;

public interface IContactUsController {

    ResponseEntity<BaseResponse> contactUs(ContactUsInputModel contactUsInputModel);
}

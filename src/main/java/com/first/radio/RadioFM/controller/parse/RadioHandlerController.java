package com.first.radio.RadioFM.controller.parse;

import com.first.radio.RadioFM.model.data.Radio;
import com.first.radio.RadioFM.service.radio.RadioService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;


@RestController
public class RadioHandlerController {

    @Autowired
    RadioService radioService;

    @PostMapping(value = "/getRadios", produces = MediaType.APPLICATION_JSON_VALUE)
    public void getRadios() throws IOException {
        Document doc = Jsoup.connect("https://mytuner-radio.com/radio/country/italy-stations?page=19").get();
        //  RadioHandlerUtil.downloadImage("https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Official_portrait_of_Sir_David_Amess_MP_crop_3.jpg/210px-Official_portrait_of_Sir_David_Amess_MP_crop_3.jpg", );
        Elements imageElements = doc.selectXpath("//*[@id='radios-list']/div[1]/ul/li/a/img");
        var ref = new Object() {
            int id = 1041;
        };
        imageElements.forEach(element -> {
            String imageUrl = element.attr("data-src");
            String alt = element.attr("alt");
            String savedFileName = alt.replaceAll(" ", "_");
            String extension =imageUrl.substring(imageUrl.length()-4);
            String filename = savedFileName + extension;
            try {
                RadioHandlerUtil.downloadImage(imageUrl,filename);
            } catch (IOException exception) {
                exception.printStackTrace();
            }


            Radio radio = new Radio();
            radio.setId(ref.id);
            radio.setCountryCode("ITA");
            radio.setCountryId(2);
            radio.setImageUrl("static/img_ita/"+filename);
            radio.setDescription(alt);
            radio.setName(alt);
            radio.setUrl("NOTVALID");
            radio.setActive(true);
          //  radioService.save(radio);
            ref.id++;

        });
        System.out.println("test");
    }
}

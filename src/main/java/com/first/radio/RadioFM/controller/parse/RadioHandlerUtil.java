package com.first.radio.RadioFM.controller.parse;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;

public class RadioHandlerUtil {
    public static void downloadImage(String sourceUrl,String filename) throws IOException {
            BufferedImage image = null;
            try {
                URL url = new URL(sourceUrl);
                image = ImageIO.read(url);
                if (image != null)
                ImageIO.write(image,"jpg",new File("C:\\images\\"+filename));
                System.out.println("write was successfully");
            } catch (IOException exception) {
                exception.printStackTrace();
                System.out.println("An Error Ocurred");
            }
    }

}

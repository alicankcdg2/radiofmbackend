package com.first.radio.RadioFM.controller.radio;

import com.first.radio.RadioFM.base.BaseResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

public interface IRadioController {
    ResponseEntity<BaseResponse> getRadios(@RequestHeader String countryCode);
}

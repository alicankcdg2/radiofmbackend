package com.first.radio.RadioFM.controller.radio;

import com.fasterxml.jackson.databind.ser.Serializers;
import com.first.radio.RadioFM.base.BaseResponse;
import com.first.radio.RadioFM.base.CacheNames;
import com.first.radio.RadioFM.model.data.Cache;
import com.first.radio.RadioFM.model.data.Radio;
import com.first.radio.RadioFM.model.input.RadioListInputModel;
import com.first.radio.RadioFM.model.input.SearchInputModel;
import com.first.radio.RadioFM.model.output.RadioListOutputModel;
import com.first.radio.RadioFM.service.cache.CacheService;
import com.first.radio.RadioFM.service.radio.RadioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/radio")
public class RadioController implements IRadioController{

    @Autowired
    private RadioService radioService;

    @Autowired
    private CacheService cacheService;

    @Override
    @GetMapping(value = "/list",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> getRadios(@RequestHeader("country_code") String countryCode) {
        BaseResponse baseResponse = new BaseResponse();
        RadioListOutputModel radioListOutputModel = radioService.getRadioListOutputModel(countryCode);
        if (radioListOutputModel != null) {
            baseResponse.setStatus(200);
            Cache cache = cacheService.getCache(countryCode, CacheNames.HOME_RADIO_LIST.toString());
            if (cache == null) {
                cache = cacheService.createCache(countryCode,CacheNames.HOME_RADIO_LIST.toString());
            }
            radioListOutputModel.setCacheCode(cache.getCode());
            radioListOutputModel.setCacheName(cache.getName());
            baseResponse.setData(radioListOutputModel);
        } else {
            baseResponse.setStatus(602);
        }

        return ResponseEntity.ok(baseResponse);
    }
}

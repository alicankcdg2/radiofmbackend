package com.first.radio.RadioFM.controller.search;

import com.first.radio.RadioFM.model.input.SearchInputModel;
import com.first.radio.RadioFM.model.output.SearchOutputModel;
import org.springframework.http.ResponseEntity;
public interface ISearchController {
    ResponseEntity<SearchOutputModel> getSearchResult(SearchInputModel searchInputModel);
}

package com.first.radio.RadioFM.controller.search;

import com.first.radio.RadioFM.base.BaseController;
import com.first.radio.RadioFM.model.data.Radio;
import com.first.radio.RadioFM.model.input.SearchInputModel;
import com.first.radio.RadioFM.model.output.SearchOutputModel;
import com.first.radio.RadioFM.service.search.SearchService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/search")
public class SearchController extends BaseController implements ISearchController {

    @Autowired
    private SearchService searchService;

    @Override
    @PostMapping(value = "/result",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SearchOutputModel> getSearchResult(@RequestBody SearchInputModel searchInputModel) {
        List<Radio> radioList = searchService.getSearchResult(searchInputModel.getSearch());
        SearchOutputModel searchOutputModel = new SearchOutputModel(200,radioList);
        return ResponseEntity.ok(searchOutputModel);
    }
}

package com.first.radio.RadioFM.controller.user;

public interface IUserController {

    String createUser(String deviceId, Integer countryId);

}

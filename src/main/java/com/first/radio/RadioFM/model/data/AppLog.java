package com.first.radio.RadioFM.model.data;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class AppLog {

  @Id
  private int id;
  private String httpLog;
  private int responseStatus;

}

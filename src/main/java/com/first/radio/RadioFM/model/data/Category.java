package com.first.radio.RadioFM.model.data;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Category {

    @Id
    private int id;
    private String name;
    private String description;
    private int active;
    private String imgUrl;

    @Transient
    private int radioSetCount;

    public int getRadioSetCount() {
        return getRadioSet().size();
    }

    @ManyToMany
    @JoinTable(
            name = "category_radio",
            joinColumns = @JoinColumn(name = "category_id"),
            inverseJoinColumns = @JoinColumn(name = "radio_id"))
    Set<Radio> radioSet;

    public Set<Radio> getRadioSet() {
        return radioSet;
    }

    public void setRadioSet(Set<Radio> radioSet) {
        this.radioSet = radioSet;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }
}

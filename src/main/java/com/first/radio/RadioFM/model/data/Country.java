package com.first.radio.RadioFM.model.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity @Data @NoArgsConstructor @AllArgsConstructor
public class Country {

    @Id @GeneratedValue (strategy = GenerationType.AUTO)
    Long id;
    String name;
    String shortName;
    int active;
}

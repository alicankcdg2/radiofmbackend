package com.first.radio.RadioFM.model.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity @Data
@NoArgsConstructor @AllArgsConstructor
public class User {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String deviceId;
    String token;
    @ManyToOne
    @JoinColumn(name="country_id")
    Country country;
}

package com.first.radio.RadioFM.model.input;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CacheControlInputModel {
    String cacheCode;
    String name;
}

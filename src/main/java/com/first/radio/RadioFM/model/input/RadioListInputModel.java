package com.first.radio.RadioFM.model.input;

public class RadioListInputModel {
    private int countryId ;

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }
}

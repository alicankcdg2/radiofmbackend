package com.first.radio.RadioFM.model.input;

public class SearchInputModel {
    private String search ;

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}

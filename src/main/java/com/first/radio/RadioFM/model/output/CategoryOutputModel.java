package com.first.radio.RadioFM.model.output;

import com.first.radio.RadioFM.model.data.Category;

import java.util.List;

public class CategoryOutputModel {
    private List<Category> categoryList;

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }
}

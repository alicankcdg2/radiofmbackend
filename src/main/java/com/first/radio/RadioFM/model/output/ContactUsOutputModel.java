package com.first.radio.RadioFM.model.output;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContactUsOutputModel {
    boolean saved;
}

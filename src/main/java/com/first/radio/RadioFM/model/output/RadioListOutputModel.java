package com.first.radio.RadioFM.model.output;

import com.first.radio.RadioFM.model.data.Cache;
import com.first.radio.RadioFM.model.data.Radio;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;

@RequiredArgsConstructor
@Getter
@Setter
public class RadioListOutputModel {
    private List<Radio> radioList;
    private String cacheCode;
    private String cacheName;
 }

package com.first.radio.RadioFM.model.output;

import com.first.radio.RadioFM.base.BaseResponse;
import com.first.radio.RadioFM.model.data.Radio;

import java.util.List;

public class SearchOutputModel extends BaseResponse {
    private List<Radio> searchResult;

    public SearchOutputModel(Integer status, List<Radio> searchResult) {
        super(status);
        this.searchResult = searchResult;
    }

    public SearchOutputModel(List<Radio> searchResult) {
        this.searchResult = searchResult;
    }

    public List<Radio> getSearchResult() {
        return searchResult;
    }

    public void setSearchResult(List<Radio> searchResult) {
        this.searchResult = searchResult;
    }
}

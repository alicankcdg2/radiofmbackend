package com.first.radio.RadioFM.repository;

import com.first.radio.RadioFM.model.data.AppLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppLogRepository extends JpaRepository<AppLog,Integer> {

    @Override
    <S extends AppLog> S save(S s);
}

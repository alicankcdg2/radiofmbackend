package com.first.radio.RadioFM.repository;

import com.first.radio.RadioFM.model.data.Cache;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CacheRepository extends JpaRepository<Cache,Integer> {
    Cache getCacheByCodeAndCountryCodeAndNameEquals(String cacheCode, String countryCode,String name);
    Cache getCacheByCountryCodeAndNameEquals(String countryCode,String name);

    @Override
    <S extends Cache> S save(S s);
}

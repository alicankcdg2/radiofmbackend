package com.first.radio.RadioFM.repository;

import com.first.radio.RadioFM.model.data.Category;
import com.first.radio.RadioFM.model.output.CategoryOutputModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category,Integer> {
    public List<Category> getCategoryByActiveEquals(int active);
}

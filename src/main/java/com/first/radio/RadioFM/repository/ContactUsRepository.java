package com.first.radio.RadioFM.repository;

import com.first.radio.RadioFM.model.data.ContactUs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactUsRepository extends JpaRepository<ContactUs,Integer> {

    @Override
    <S extends ContactUs> S save(S s);

    @Override
    ContactUs getById(Integer integer);
}

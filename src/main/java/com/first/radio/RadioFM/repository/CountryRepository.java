package com.first.radio.RadioFM.repository;

import com.first.radio.RadioFM.model.data.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<Country,Long> {
    Country getCountryByShortName(String shortName);
}

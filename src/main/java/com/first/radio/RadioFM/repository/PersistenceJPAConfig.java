package com.first.radio.RadioFM.repository;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "com.first.radio.RadioFM.repository")
class PersistenceJPAConfig {}


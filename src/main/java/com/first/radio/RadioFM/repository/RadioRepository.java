package com.first.radio.RadioFM.repository;

import com.first.radio.RadioFM.model.data.Radio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RadioRepository extends JpaRepository<Radio,Integer> {

    List<Radio> findRadioByActiveAndCountryCodeEqualsAndUrlNotContaining(boolean active,String countryCode,String notValid);
}

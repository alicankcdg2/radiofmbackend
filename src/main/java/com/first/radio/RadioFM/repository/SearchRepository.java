package com.first.radio.RadioFM.repository;

import com.first.radio.RadioFM.model.data.Radio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SearchRepository extends JpaRepository<Radio,Integer> {
    List<Radio> findByNameContaining(@Param("search") String search);

}

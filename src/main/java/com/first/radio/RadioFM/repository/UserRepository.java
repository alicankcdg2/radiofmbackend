package com.first.radio.RadioFM.repository;
import com.first.radio.RadioFM.model.data.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
    User getByDeviceId(String deviceId);
    User getUserById(Long id);
}

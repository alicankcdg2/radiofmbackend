package com.first.radio.RadioFM.service.applog;

import com.first.radio.RadioFM.model.data.AppLog;
import com.first.radio.RadioFM.repository.AppLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppLogService implements IAppLogService{
    @Autowired
    private AppLogRepository appLogRepository;


    @Override
    public AppLog save(AppLog appLog) {
        AppLog appLog1 = appLogRepository.save(appLog);
        return appLog1;
    }

    @Override
    public AppLog get(String guid) {
        return null;
    }

    @Override
    public AppLog get(int id) {
        return null;
    }
}

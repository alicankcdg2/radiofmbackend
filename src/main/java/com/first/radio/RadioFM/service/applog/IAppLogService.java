package com.first.radio.RadioFM.service.applog;

import com.first.radio.RadioFM.model.data.AppLog;

public interface IAppLogService {
    AppLog save(AppLog appLog);
    AppLog get(String guid);
    AppLog get(int id);
}

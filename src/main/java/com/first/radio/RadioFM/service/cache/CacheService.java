package com.first.radio.RadioFM.service.cache;

import com.first.radio.RadioFM.model.data.Cache;
import com.first.radio.RadioFM.repository.CacheRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CacheService implements ICacheService{

    @Autowired
    CacheRepository cacheRepository;

    @Override
    public boolean isCacheChange(String cacheCode,String countryCode,String name) {
        Cache cache = cacheRepository.getCacheByCodeAndCountryCodeAndNameEquals(cacheCode,countryCode,name);
        return cache == null;
    }

    @Override
    public Cache getCache (String countryCode, String name) {
        Cache cache = cacheRepository.getCacheByCountryCodeAndNameEquals(countryCode,name);
        return cache;
    }

    @Override
    public Cache createCache (String countryCode, String name) {
        Cache cache = new Cache();
        cache.setName(name);
        cache.setCountryCode(countryCode);
        cache.setCode(UUID.randomUUID().toString());
        Cache cache1 = cacheRepository.save(cache);
        return cache1;
    }
}

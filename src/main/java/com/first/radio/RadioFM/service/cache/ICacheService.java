package com.first.radio.RadioFM.service.cache;

import com.first.radio.RadioFM.model.data.Cache;
import org.springframework.stereotype.Service;

public interface ICacheService {
    boolean isCacheChange(String cacheCode,String countryCode,String name);
    Cache getCache(String countryCode, String name);
    Cache createCache(String countryCode, String name);
}

package com.first.radio.RadioFM.service.category;

import com.first.radio.RadioFM.model.data.Category;
import com.first.radio.RadioFM.model.output.CategoryOutputModel;
import com.first.radio.RadioFM.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CategoryService implements ICategoryService{

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public CategoryOutputModel getCategoryList() {
        CategoryOutputModel categoryOutputModel = new CategoryOutputModel();
        categoryOutputModel.setCategoryList(categoryRepository.getCategoryByActiveEquals(1));
        return categoryOutputModel;
    }
}

package com.first.radio.RadioFM.service.contactus;

import com.first.radio.RadioFM.model.data.ContactUs;
import com.first.radio.RadioFM.repository.ContactUsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContactUsService implements IContactUsService{

    @Autowired
    ContactUsRepository contactUsRepository;

    @Override
    public boolean saveMessage(String subject, String content) {
        ContactUs contactUs = new ContactUs();
        contactUs.setSubject(subject);
        contactUs.setContent(content);
        contactUsRepository.save(contactUs);
        return contactUsRepository.save(contactUs) != null;
    }
}

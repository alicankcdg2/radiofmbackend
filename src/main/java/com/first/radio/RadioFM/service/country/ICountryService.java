package com.first.radio.RadioFM.service.country;

import com.first.radio.RadioFM.model.data.Country;

public interface ICountryService {
    Country getByShortName(String shortName);
}

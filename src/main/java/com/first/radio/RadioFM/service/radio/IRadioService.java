package com.first.radio.RadioFM.service.radio;

import com.first.radio.RadioFM.model.data.Radio;
import com.first.radio.RadioFM.model.output.RadioListOutputModel;

public interface IRadioService {
    RadioListOutputModel getRadioListOutputModel(String countryCode);
    void save(Radio radio);
}

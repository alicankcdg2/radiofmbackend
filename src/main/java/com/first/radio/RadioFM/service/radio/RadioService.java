package com.first.radio.RadioFM.service.radio;

import com.first.radio.RadioFM.model.data.Radio;
import com.first.radio.RadioFM.model.output.RadioListOutputModel;
import com.first.radio.RadioFM.repository.RadioRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service @Slf4j
public class RadioService implements IRadioService{

    @Autowired
    private RadioRepository radioRepository;

    @Override
    public RadioListOutputModel getRadioListOutputModel(String countryCode) {
        RadioListOutputModel radioListOutputModel = new RadioListOutputModel();
        radioListOutputModel.setRadioList(radioRepository.findRadioByActiveAndCountryCodeEqualsAndUrlNotContaining(true,countryCode,"NOTVALID"));
        return radioListOutputModel;
    }

    @Override
    public void save(Radio radio) {
       radioRepository.save(radio);
    }


}

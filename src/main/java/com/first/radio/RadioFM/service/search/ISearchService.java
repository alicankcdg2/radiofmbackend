package com.first.radio.RadioFM.service.search;

import com.first.radio.RadioFM.model.data.Radio;

import java.util.List;


public interface ISearchService {
    List<Radio> getSearchResult(String searchFilter);
}

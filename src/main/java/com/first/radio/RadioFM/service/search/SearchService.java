package com.first.radio.RadioFM.service.search;

import com.first.radio.RadioFM.model.data.Radio;
import com.first.radio.RadioFM.repository.SearchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Configurable
public class SearchService implements ISearchService {

    @Autowired
    private SearchRepository searchRepository;

    @Override
    public List<Radio> getSearchResult(String searchFilter) {
        List<Radio> radioList = searchRepository.findByNameContaining(searchFilter);
        return radioList;
    }
}

package com.first.radio.RadioFM.service.user;

import com.first.radio.RadioFM.model.data.User;

import java.util.List;

public interface IUserService {
    User getByDeviceId(String deviceId);
    User getUserById(Long id);
    User saveUser(User user);
    List<User> getUsers();
}

package com.first.radio.RadioFM.service.user;

import com.first.radio.RadioFM.model.data.User;
import com.first.radio.RadioFM.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service @Slf4j @RequiredArgsConstructor @Transactional
public class UserService implements IUserService{

    private final UserRepository userRepository;

    @Override
    public User getByDeviceId(String deviceId) {
        ;
        return null;
    }

    @Override
    public User getUserById(Long id) {
        return null;
    }

    @Override
    public User saveUser(User user) {
        return null;
    }

    @Override
    public List<User> getUsers() {
        return null;
    }
}
